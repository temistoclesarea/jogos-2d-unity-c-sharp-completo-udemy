﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inputs3 : MonoBehaviour 
{
	
	void Update () 
	{

		float H = Input.GetAxis("Horizontal");
		float V = Input.GetAxis("Vertical");

		//float H = Input.GetAxis("Mouse X");
		//float V = Input.GetAxis("Mouse Y");

		transform.Translate(new Vector3 (H * Time.deltaTime, V * Time.deltaTime,0));
		
	}
}
