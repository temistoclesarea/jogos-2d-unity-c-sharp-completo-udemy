﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inputs2 : MonoBehaviour 
{

	public float vel = 2.5f;

	void Update () 
	{

		//Forma 1 de movimento
		if (Input.GetKey(KeyCode.RightArrow)) { 
			transform.Translate(new Vector3(vel * Time.deltaTime, 0, 0));
		}
		if (Input.GetKey(KeyCode.LeftArrow)) { 
			transform.Translate(new Vector3(-vel * Time.deltaTime, 0, 0));
		}
		if (Input.GetKey(KeyCode.UpArrow)) { 
			transform.Translate(new Vector3(0, vel * Time.deltaTime, 0));
		}
		if (Input.GetKey(KeyCode.DownArrow)) { 
			transform.Translate(new Vector3(0, -vel * Time.deltaTime, 0));
		}
		
	}
}
