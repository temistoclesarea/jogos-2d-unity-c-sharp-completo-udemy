﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inputs1 : MonoBehaviour 
{

	void Update () 
	{
		if (Input.GetKeyUp(KeyCode.RightArrow)) { // somente quando solta o botão
			print("GetKeyUp - Right Arrow");
		}
		if (Input.GetKeyDown(KeyCode.LeftArrow)) { // somente quando aperta o botão
			print("GetKeyDown - Left Arrow");
		}
		if (Input.GetKey(KeyCode.DownArrow)) { // repete a acão ao deixar precionar
			print("GetKey - Down Arrow");
		}
		
	}
}
