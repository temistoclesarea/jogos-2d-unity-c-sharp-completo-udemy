﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Inputs4 : MonoBehaviour 
{
	
	void Update () 
	{

		float H = Input.GetAxis("Mouse X");
		float V = Input.GetAxis("Mouse Y");

		//float H = Input.GetAxis("Mouse ScrollWheel"); 

		//float H = Input.GetAxis("Fire1");
		//float V = Input.GetAxis("Fire2");

		//float H = Input.GetAxis("Fire3");

		transform.Translate(new Vector3 (H * Time.deltaTime, V * Time.deltaTime,0));
		
	}
}
