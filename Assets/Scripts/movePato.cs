﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movePato : MonoBehaviour {

	public float vel = 2.5f;
	public int moedas = 0;
	public GameObject efeitoCoin;
	public AudioClip somCoin;

	void Start () {
		
	}
	
	
	void Update () {
		if (Input.GetKey(KeyCode.RightArrow)) {
			transform.Translate(new Vector2(vel * Time.deltaTime, 0));
		}
		if (Input.GetKey(KeyCode.LeftArrow)) {
			transform.Translate(new Vector2(-vel * Time.deltaTime, 0));
		}

		//print(moedas);
		
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.CompareTag("moeda")) {
			//Instantiate(efeitoCoin, new Vector3(this.gameObject.transform.position.x, this.gameObject.transform.position.y, this.gameObject.transform.position.z), Quaternion.identity);
			Gerenciador.inst.PlayAudio(somCoin);
			moedas++;
			Destroy(other.gameObject);
		}
	}
}
