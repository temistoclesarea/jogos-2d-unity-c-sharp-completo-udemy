﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class moviment : MonoBehaviour {
	
	public float velX = 0.005f;
	public float velY = 0.005f;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.Translate(new Vector3(velX, velY, 0)); //movimentação
		//transform.Rotate(new Vector3(0,0,0.5f)); //rotação
		//transform.localScale += new Vector3(0.1f,0.1f,0); // escala
	}
}
