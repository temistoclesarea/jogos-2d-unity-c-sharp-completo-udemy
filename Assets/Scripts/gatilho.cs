﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class gatilho : MonoBehaviour {

	public bool liberaRot;
	public GameObject ima;
	
	void Start () {
		liberaRot = false;
	}
	
	
	void Update () {
		if (liberaRot == true) {
			ima.gameObject.transform.Rotate(new Vector3(0,0,5 * Time.deltaTime));
		}
	}

	void OnTriggerEnter2D(Collider2D other) {
		if (other.gameObject.CompareTag("ave")) {
			liberaRot = true;
		}
	}

	void OnTriggerExit2D(Collider2D other) {
		if (other.gameObject.CompareTag("ave")) {
			liberaRot = false;
		}
	}
}
