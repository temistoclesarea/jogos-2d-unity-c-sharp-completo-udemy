﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pulo : MonoBehaviour {

	public float forca = 500f;
	public Rigidbody2D bola;
	public bool liberaPulo = false;
	public int duplo = 2;
	public AudioClip puloSom;
	
	void Start () {
		
	}
	
	void Update () {
		//if (liberaPulo == true) {
		if (duplo > 0) {
			if (Input.GetKeyDown(KeyCode.Space)) {
				bola.AddForce(new Vector2(0, forca * Time.deltaTime), ForceMode2D.Impulse);
				Gerenciador.inst.PlayAudio(puloSom);
				duplo--;
			}
		}
	}

	private void OnCollisionEnter2D(Collision2D other) {
		if (other.gameObject.CompareTag("chao")) {
			duplo = 2;
			//liberaPulo = true;
		}
	}

	
	/*private void OnCollisionExit2D(Collision2D other) {
		if (other.gameObject.CompareTag("chao")) {
			liberaPulo = false;
		}
	}*/
}
